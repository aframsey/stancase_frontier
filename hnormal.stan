functions {
   
   real hnormal_lpdf(vector y, real sigma) {

      int N = rows(y);

      vector[N] prob;
      real lprob;
      for (i in 1:N) {
         prob[i] = log(2*sigma)-log(pi())-(y[i]^2*sigma^2)/pi();
      }
      lprob = sum(prob);
      return lprob;
   }

}

data {
   int<lower=0> N; // Number of observations
   int<lower=0> K; // Number of factors of cost function
   matrix[N, K] x; // Matrix of factors of cost function
   vector[N] y;    // Output
}
parameters {
   real alpha;            // Intercept
   vector[K] beta;        // Coefficients of the cost function
   real<lower=0> sigma;   // Error variance
   real<lower=0> lambda;  // Parameter for half-normal inefficiency distribution
   vector<lower=0>[N] u;  // Inefficiency terms
}
model {
   alpha ~ normal(0, 100);
   beta ~ normal(0, 100);
   sigma ~ inv_gamma(0.001, 0.001);
   lambda ~ gamma(1, 1/37.5);
   u ~ hnormal(lambda);
   y ~ normal(alpha + x * beta + u, sigma);
}
generated quantities {
   vector[N] eff;
   eff = exp(-u);
}
