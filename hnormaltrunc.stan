data {
   int<lower=0> N; // Number of observations
   int<lower=0> K; // Number of factors of cost function
   matrix[N, K] x; // Matrix of factors of cost function
   vector[N] y;    // Vector of costs
}
parameters {
   real alpha;            // Intercept
   vector[K] beta;        // Coefficients of the cost function
   real<lower=0> sigma;   // Error standard deviation
   real<lower=0> lambda;  // Parameter for inefficiency distribution
   vector<lower=0>[N] u;  // Inefficiency terms
}
model {
   alpha ~ normal(0, 100);
   beta ~ normal(0, 100);
   sigma ~ inv_gamma(0.001, 0.001);
   lambda ~ gamma(1, 1/37.5);
   for (n in 1:N) {
      u[n] ~ normal(0, 1/((lambda*sqrt(2))/sqrt(pi())))T[0,];
   }
   y ~ normal(alpha + x * beta + u, sigma);
}
generated quantities {
   vector[N] eff;
   eff = exp(-u);
}
