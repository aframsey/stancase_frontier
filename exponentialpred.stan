data {
   int<lower=0> N; // Number of observations
   int<lower=0> K; // Number of factors of cost function
   matrix[N, K] x; // Matrix of factors of cost function
   vector[N] y;    // Vector of costs
}
parameters {
   real alpha;             // Intercept
   vector[K] beta;         // Coefficients of the cost function
   real<lower=0> sigma;    // Error standard deviation
   real<lower=0> lambda;   // Parameter for inefficiency distribution
   vector<lower=0>[N] u;   // Inefficiency terms
}
model {
   alpha ~ normal(0, 100);
   beta ~ normal(0, 100);
   sigma ~ inv_gamma(0.001, 0.001);
   lambda ~ exponential(-log(0.875));
   u ~ exponential(lambda);
   y ~ normal(alpha + x * beta + u, sigma);
}
generated quantities {
   vector[N] eff;
   real predu;
   real predeff;
   eff = exp(-u);
   predu = exponential_rng(lambda);
   predeff = exp(-predu);
}