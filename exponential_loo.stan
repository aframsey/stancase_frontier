functions {
   real nexp_llike(real y, row_vector x, real alpha, vector beta, real sigma, real lambda) {  
      real prob;
      real sigu = 1/lambda;
      real theta = sigu/sigma;
      real epsi = y - alpha - x * beta;
      prob = -log(sigu) + (sigma^2)/(2*sigu^2) + normal_lcdf((epsi - (sigma^2)/sigu)/sigma|0,1) - (epsi/sigu); 
      return prob;
   }
}
data {
   int<lower=0> N; // Number of observations
   int<lower=0> K; // Number of factors of cost function
   matrix[N, K] x; // Matrix of factors of cost function
   vector[N] y;    // Output
}
parameters {
   real alpha;            // Intercept
   vector[K] beta;        // Coefficients of the cost function
   real<lower=0> sigma;   // Error variance
   real<lower=0> lambda;  // Inverse scale of inefficiency distribution
   vector<lower=0>[N] u;  // Inefficiency terms
}
model {
   alpha ~ normal(0, 100);
   beta ~ normal(0, 100);
   sigma ~ inv_gamma(0.001, 0.001);
   lambda ~ exponential(-log(0.875)); // Prior median 
   u ~ exponential(lambda);
   y ~ normal(alpha + x * beta + u, sigma);
}
generated quantities {
   vector[N] eff;
   vector[N] log_lik;
   eff = exp(-u);
   for (i in 1:N) {
      log_lik[i] = nexp_llike(y[i], x[i], alpha, beta, sigma, lambda);
   }
}
